// ------------------------------------------------------
// Generating mesh for effective model
//
// Written by 
// Y. Sudhakar (sudhakar@iitgoa.ac.in)
// Sahaj Jain (sahaj.jain.16003@iitgoa.ac.in)
// School of Mechanical Sciences, IIT Goa
//
// Tested with Gmsh 4.8.4
// -----------------------------------------------------
//====================================
// (inner) Radius of the circle
// the surface of which will be filled with roughness
R = 10.0;
Dia = 2.0*R;

// roughness height
hei = 1.0;

// Dimension of the box
xmin = -7.5*Dia;
xmax = 25.0*Dia;
ymin = -1.5*Dia;
ymax = -ymin;
//xmin = -15.0;
//xmax = 15.0;
//ymin = -15.0;
//ymax = 15.0;

// Mesh spacing
h = 0.005*R;// 0.01*R;      // finest mesh -- on cylinder surface
h1 = 0.075*R;// 0.05*R;      // medium mesh -- on inner box
h2 = 0.4*R;// 0.5*R;     // coarse mesh -- on outer box
//====================================

For jj In {1:4}
  // set interface height
  If( jj == 1)
    intf = 0.05;
  ElseIf( jj == 2)
    intf = 0.1;
  ElseIf( jj == 3)
    intf = 0.2;
  ElseIf( jj == 4)
    intf = 0.3;
  EndIf


  // centre of the circle
  Point(99) = {0,0,0,h};

  // end points of the circle
  Point(1)={(R+hei+intf),0,0,h};
  Point(2)={0,(R+hei+intf),0,h};
  Point(3)={-(R+hei+intf),0,0,h};
  Point(4)={0,-(R+hei+intf),0,h};

  Circle(101)={1,99,4};
  Circle(102)={4,99,3};
  Circle(103)={3,99,2};
  Circle(104)={2,99,1};
  Line Loop(11) = {101:104};
 

  //--- Create the box containing the cylinder
  Point(701) = {ymin,  ymin, 0, h1};
  Point(702) = {4*Dia, ymin, 0, h1};
  Point(703) = {4*Dia, ymax, 0, h1};
  Point(704) = {ymin,  ymax, 0, h1};
  Line(501) = {701,702};
  Line(502) = {702,703};
  Line(503) = {703,704};
  Line(504) = {704,701};
  Line Loop(12) = {501:504};

  //--- Create the outer box
  Point(1001) = {xmax,ymin,0,h2};
  Point(1002) = {xmax,ymax,0,h2};
  Point(1003) = {xmin,ymax,0,h2};
  Point(1004) = {xmin,ymin,0,h2};
  Line(1001) = {1004,701};
  Line(1002) = {704, 1003};
  Line(1003) = {1003,1004};

  Line(1004) = {702, 1001};
  Line(1005) = {1001,1002};
  Line(1006) = {1002,703};

  // box in the upstream
  Line Loop(13) = {1001,-504,1002,1003};
  // box in the downstream
  Line Loop(14) = {1004:1006,-502};

  Plane Surface(1) = {12,-11};
  Plane Surface(2) = {13};
  Plane Surface(3) = {14};
  Physical Surface(1)={1,2,3};

  //--- Boundary conditions
  // 1 -- velocity inlet
  // 2 -- rough cylinder surface
  // 3 -- right
  // 4 -- top and bottom
  Physical Line(1) = {1003};
  Physical Line(2) = {101:104};
  Physical Line(3) = {1005};
  Physical Line(4) = {1001,1002,1004,1006,501,503};

  //--- Create mesh
  Mesh 2;
  Save Sprintf("cyl-%.2g.msh", jj);
  Delete Model;
EndFor

//----------------------------------------------------------------
// Computation of constitutive parameters in polar coordinates 
// for rough surfaces with square roughness elements
//
// Written by 
// Y. Sudhakar (sudhakar@iitgoa.ac.in)
// Sahaj Jain (sahaj.jain.16003@iitgoa.ac.in)
// School of Mechanical Sciences, IIT Goa
//
// Tested with Freefem++ version 4.6
//----------------------------------------------------------------

load "gmsh"
load "UMFPACK64"
load "lapack"

// Reduce output to minimum
verbosity = 0;

// ---------------- set this parameter ---------------------------
// interface height (absolute value)
// 1,2,3,4 implies 0.05, 0.1, 0.2, 0.3
int intfNum = 4;
// angle occupied by the microstructure
real ang = 10*pi/180.0;
//----------------------------------------------------------------

// interface height
real iloc;

// load mesh created using gmsh
mesh Th;
if (intfNum == 1)
{
	iloc = 0.05;
	Th = gmshload("cyl-0.05.msh");
}
if (intfNum == 2)
{
	iloc = 0.1;
	Th = gmshload("cyl-0.1.msh");
}
if (intfNum == 3)
{
	iloc = 0.2;
	Th = gmshload("cyl-0.2.msh");
}
if (intfNum == 4)
{
	iloc = 0.3;
	Th = gmshload("cyl-0.3.msh");
}

// radius at the interface
real Ri = 11.0 + iloc;

plot(Th);

fespace Uh(Th,[P2,P2,P1],periodic=[[2,y],[3,y],[x,2],[x,3]]);
Uh [u,v,p], [uuc,vvc,ppc];

// Define FE functions for solution storage of K and L problem
Uh [uL,vL,pL];

func cost=x/sqrt(x*x+y*y);
func sint=y/sqrt(x*x+y*y);

// Define the problem for the slip
problem stokesLc([u,v,p],[uuc,vvc,ppc],solver=sparsesolver) =
    int2d(Th)( - (-p + 2.0*dx(u))*dx(uuc) - (dy(u)+dx(v))*dy(uuc)
                - (-p + 2.0*dy(v))*dy(vvc) - (dx(v)+dy(u))*dx(vvc)
                + (dx(u)+dy(v))*ppc  )
    + int1d(Th,5)( sint*uuc - cost*vvc )
    + on(1,u=0,v=0);

// Solve the slip problem, do the post-processing
stokesLc; uL[] = u[];

//plot(u);
//plot(v);
//plot(v);
//plot(p);

// --- computation of slip and transpiration coefficiet
fespace Ugrad(Th,P2);
Ugrad ur, ut;
ut = sint*u - cost*v;
ur = cost*u + sint*v;
real slip = int1d(Th,5)(ut)/int1d(Th,5)(1.0);

// --- load only the interior part of mesh
// which was generated using gmsh
// this is used to compute M
mesh Thi;
if (intfNum == 1)
{
	Thi = gmshload("intCyl-0.05.msh");
}
if (intfNum == 2)
{
	Thi = gmshload("intCyl-0.1.msh");
}
if (intfNum == 3)
{
	Thi = gmshload("intCyl-0.2.msh");
}
if (intfNum == 4)
{
	Thi = gmshload("intCyl-0.3.msh");
}
fespace Uhi(Thi,P2);
Uhi uri,uti;
matrix IV34= interpolate(Uhi,Ugrad);
uri[] = IV34*ur[];
uti[] = IV34*ut[];
//plot(uti);
//plot(uri);

plot(ut);
plot(uti);


real tran = int2d(Thi)(uti/(sqrt(x*x+y*y)))/slip/ang/Ri;
cout <<"slip          = " << slip <<endl;
cout <<"transpiration = " << tran <<endl;

// --- computation of drag and stress correction factors
//fespace Ugrad(Th,P2);
Ugrad dudx, dudy, dvdx;
dudx = dx(u);
dudy = dy(u);
dvdx = dx(v);
real pdrag = -int1d(Th,1)( -p*N.x ) / int1d(Th,5)(1.0);
real vdrag = -int1d(Th,1)( 2*dudx*N.x + (dudy+dvdx)*N.y ) / int1d(Th,5)(1.0);
cout << "pdrag = " << pdrag << endl;
cout << "vdrag = " << vdrag << endl;

ofstream outfile("coef-microscale.txt",append);
outfile <<iloc <<"\t"<<slip<<"\t"<<tran<<"\t"<<pdrag<<"\t"<<vdrag<<"\n";

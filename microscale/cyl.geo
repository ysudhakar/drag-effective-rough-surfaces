// -----------------------------------------------------
// Creation of computational domain and mesh for 
// the microscale problem in polar coordinates.
//
// Written by 
// Y. Sudhakar (sudhakar@iitgoa.ac.in)
// Sahaj Jain (sahaj.jain.16003@iitgoa.ac.in)
// School of Mechanical Sciences, IIT Goa
//
// Tested with Gmsh 4.8.4
// -----------------------------------------------------
//====================================
// (inner) Radius of the circle
// the surface of which will be filled with roughness
R = 10;

// roughness height
hei = 1.0;

// interface height 
ift = 0.3*hei;

// angle occupied by the roughness element
nummicro = 36;
angL = 2.0*Pi/nummicro; // value of epsilon = 0.1

// angle formed by the square roughness
angEll = 5.0*Pi/180.0;

// Mesh spacing
h1=0.05*R;
h=0.001*R;//0.004*boxx;
//====================================


startAng = 0.5*(Pi-angL);
ang1 = startAng;
ang2 = ang1 + 0.5*(angL-angEll);
ang3 = ang2 + angEll;
ang4 = ang1 + angL;


// centre of the circle
Point(99) = {0,0,0,h};

// Points on the solid surface -- circle with roughness
Point(1)={R*Cos(ang1),R*Sin(ang1),0,h};
Point(2)={R*Cos(ang2),R*Sin(ang2),0,h};
Point(3)={(R+hei)*Cos(ang2),(R+hei)*Sin(ang2),0,h};
Point(4)={(R+hei)*Cos(ang3),(R+hei)*Sin(ang3),0,h};
Point(5)={R*Cos(ang3),R*Sin(ang3),0,h};
Point(6)={R*Cos(ang4),R*Sin(ang4),0,h};
Circle(101) = {6, 99, 5};
Line(102) = {5,4};
Circle(103) = {4, 99, 3};
Line(104) = {3,2};
Circle(105) = {2, 99, 1};

// Points on the interface
Point(7)={(R+hei+ift)*Cos(ang1),(R+hei+ift)*Sin(ang1),0,h};
Point(8)={(R+hei+ift)*Cos(ang4),(R+hei+ift)*Sin(ang4),0,h};
Line(106) = {1,7};
Circle(107) = {7,99,8};
Line(108) = {8,6};

// Points on the outer surface
circum = (R+hei+ift)*angL;
hei_outer = 5.0*circum;
Point(9)={(R+hei+ift+hei_outer)*Cos(ang1),(R+hei+ift+hei_outer)*Sin(ang1),0,h1};
Point(10)={(R+hei+ift+hei_outer)*Cos(ang4),(R+hei+ift+hei_outer)*Sin(ang4),0,h1};
Line(109) = {7,9};
Circle(110) = {9,99,10};
Line(111) = {10,8};

Line Loop(1) = {101:108};
Line Loop(2) = {109:111,-107};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Physical Surface(1)={1};

Mesh 2;
Save Sprintf("intCyl-%.2g.msh", ift);
Delete Physicals;


Physical Surface(1)={1,2};
Periodic Line{108} = {106} Rotate {{0,0,1},{0,0,0},angL};
Periodic Line{111} = {109} Rotate {{0,0,1},{0,0,0},angL};

// Here are the following physical line numbers
// 1 -- no-slip
// 2 -- periodic right
// 3 -- periodic left
// 4 -- top surface
// 5 -- interface
Physical Line(1)={101:105}; 
Physical Line(2)={108,111}; 
Physical Line(3)={106,109}; 
Physical Line(4)={110}; 
Physical Line(5)={107}; 

Mesh 2;
Save Sprintf("cyl-%.2g.msh", ift);

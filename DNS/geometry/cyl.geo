// -------------------------------------------------------------
// GMSH file used to generate meshes for performing sample
// DNS simulations. The roughness element is rotated in
// each subsequent mesh file such that after "num" rotations
// the angle occupied by one roughness element is covered.
//
// Written by 
// Y. Sudhakar (sudhakar@iitgoa.ac.in)
// Sahaj Jain (sahaj.jain.16003@iitgoa.ac.in)
// School of Mechanical Sciences, IIT Goa
//
// Tested with Gmsh 4.8.4
// ------------------------------------------------------------

//====================================
// (inner) Radius of the circle
// the surface of which will be filled with roughness
R = 10.0;
Dia = 2.0*R;

// roughness height
hei = 1.0;

// angle occupied by the roughness element
nummicro = 36;
angL = 2.0*Pi/nummicro;

// angle formed by the ellipse
angEll = 0.5*angL;

// Dimension of the box
xmin = -7.5*Dia;
xmax = 25.0*Dia;
ymin = -1.5*Dia;
ymax = -ymin;
//xmin = -15.0;
//xmax = 15.0;
//ymin = -15.0;
//ymax = 15.0;

// Mesh spacing
h = 0.005*R;// 0.01*R;      // finest mesh -- on cylinder surface
h1 = 0.075*R;// 0.05*R;      // medium mesh -- on inner box
h2 = 0.4*R;// 0.5*R;     // coarse mesh -- on outer box

// number of files
num = 50;
//====================================

For jj In {0:num-1}
  startAng = (jj * angL/num);
	//Printf("Startang %g ", startAng*180.0/Pi);

	//--- create one microscale geometry
	ang1 = startAng;
	ang2 = ang1 + 0.5*(angL-angEll);
	ang3 = ang2 + angEll;
	ang4 = ang1 + angL;

	ange = ang1+angL/2.0; // to get centre point of ellipse

	// centre of the circle
	Point(99) = {0,0,0,h};

    // Points on the solid surface -- circle with roughness
    Point(1)={R*Cos(ang1),R*Sin(ang1),0,h};
    Point(2)={R*Cos(ang2),R*Sin(ang2),0,h};
    Point(3)={(R+hei)*Cos(ang2),(R+hei)*Sin(ang2),0,h};
    Point(4)={(R+hei)*Cos(ang3),(R+hei)*Sin(ang3),0,h};
    Point(5)={R*Cos(ang3),R*Sin(ang3),0,h};
    Point(6)={R*Cos(ang4),R*Sin(ang4),0,h};

    Circle(101) = {6, 99, 5};
	Line(102) = {5,4};
    Circle(103) = {4, 99, 3};
	Line(104) = {3,2};
    Circle(105) = {2, 99, 1};

	//--- copy and rotate the same to create the full circle
	For nn In {0:nummicro-1}
		Rotate {{0,0,1}, {0,0,0}, nn*angL} { Duplicata{ Line{101:105}; }}
	EndFor
	Line Loop(11) = {101:280};


	//--- Create the box containing the cylinder
	Point(701) = {ymin,  ymin, 0, h1};
	Point(702) = {4*Dia, ymin, 0, h1};
	Point(703) = {4*Dia, ymax, 0, h1};
	Point(704) = {ymin,  ymax, 0, h1};
	Line(501) = {701,702};
	Line(502) = {702,703};
	Line(503) = {703,704};
	Line(504) = {704,701};
	Line Loop(12) = {501:504};

	//--- Create the outer box
	Point(1001) = {xmax,ymin,0,h2};
	Point(1002) = {xmax,ymax,0,h2};
	Point(1003) = {xmin,ymax,0,h2};
	Point(1004) = {xmin,ymin,0,h2};
	Line(1001) = {1004,701};
	Line(1002) = {704, 1003};
	Line(1003) = {1003,1004};

	Line(1004) = {702, 1001};
	Line(1005) = {1001,1002};
	Line(1006) = {1002,703};
//	Line Loop(13) = {1001:1004};

    // box in the upstream
    Line Loop(13) = {1001,-504,1002,1003};
	// box in the downstream
    Line Loop(14) = {1004:1006,-502};

	Plane Surface(1) = {12,-11};
	Plane Surface(2) = {13};
	Plane Surface(3) = {14};
	Physical Surface(1)={1,2,3};

  //--- Boundary conditions
  // 1 -- velocity inlet
  // 2 -- rough cylinder surface
  // 3 -- right
  // 4 -- top and bottom
  Physical Line(1) = {1003};
  Physical Line(2) = {101:280};
  Physical Line(3) = {1005};
  Physical Line(4) = {1001,1002,1004,1006,501,503};

  //--- Create mesh
  Mesh 2;
  Save Sprintf("cyl-%.2g.msh", jj+1);
  Delete Model;
EndFor

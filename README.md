### Overview
This repository contains source codes for performing effective simulation of flows over rough cylinders. Also contain source codes to perform geometry-resolved simulations for generating reference data and the microscale problem to compute constitutive coefficients.

These source codes can reprouce results published in the following paper

*S. Jain, Y. Sudhakar, Prediction of drag components on rough surfaces using effective models, Physics of Fluids:34 (2022) 073602.*

It contains the following folders

1. DNS -- Geometry resolved simulations of cylinder with square roughness elements
2. microscale -- problem used to compute constitutive parameters of square cylinder in polar coordinates
3. effective -- code for effective simulation

The files are tested with Gmsh 4.8.4 and Freefem++ 4.6.

---

### DNS folder
The source code given here will perform DNS of flow over rough cylinders and also carryout ensembled averaging

**Step 1:** Generate mesh files for all sample simulations by executing the following command within the geometry folder. This will create 50 mesh files that will be used to perform DNS simulations and to carryout ensembled averaging

_gmsh -2 cyl.geo -format msh2_

**Step 2:** Run the code from this folder. This will read all 50 input files, and write ensembled averaged data

_Freefem++ dns.edp_

---

### Microscale folder
Contains source codes to generate mesh in the computational domain and to solve the microscale problem with square roughness elements to compute constitutive parameters

**Step 1:** Generate mesh using the following command. This will create two output files, one for the whole domain and one for the domain below the interface. The later domain is used for the computation of **M**.

*gmsh -2 cyl.geo -format msh2*

**Step 2:** Solve the microscale problem and write constitutive coefficients in an output file

*Freefem++ micro.edp*

---

### effective folder
Contains source code to for the effective simulations. Commands used in the *microscale* folder can be used to run the simulations.
